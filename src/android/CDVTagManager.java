/**
 * Copyright (c) 2014 Jared Dickson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package dk.ovdal.tagmanager;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;

import com.google.analytics.tracking.android.GAServiceManager;
import com.google.tagmanager.Container;
import com.google.tagmanager.ContainerOpener;
import com.google.tagmanager.ContainerOpener.OpenType;
import com.google.tagmanager.DataLayer;
import com.google.tagmanager.TagManager;

import android.app.AlertDialog;
import android.content.DialogInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class echoes a string called from JavaScript.
 */
public class CDVTagManager extends CordovaPlugin {

    private Container mContainer;
        private boolean inited = false;
        // variable to hold context
        // private Context context;

        public CDVTagManager() {
        }

        public void initialize(CordovaInterface cordova, CordovaWebView webView) {
            super.initialize(cordova, webView);
        }

        @Override
        public boolean execute(String action, JSONArray args, CallbackContext callback) {
            if (action.equals("initGTM")) {
                try {
                    // Set the dispatch interval
                    GAServiceManager.getInstance().setLocalDispatchPeriod(args.getInt(1));

                    TagManager tagManager = TagManager.getInstance(this.cordova.getActivity().getApplicationContext());
                    ContainerOpener.openContainer(
                                                  tagManager,                             // TagManager instance.
                                                  args.getString(0),                      // Tag Manager Container ID.
                                                  OpenType.PREFER_NON_DEFAULT,            // Prefer not to get the default container, but stale is OK.
                                                  null,                                   // Time to wait for saved container to load (ms). Default is 2000ms.
                                                  new ContainerOpener.Notifier() {        // Called when container loads.
                        @Override
                        public void containerAvailable(Container container) {
                            // Handle assignment in callback to avoid blocking main thread.
                            mContainer = container;
                            inited = true;
                        }
                    }
                                                  );
                    callback.success("initGTM - id = " + args.getString(0) + "; interval = " + args.getInt(1) + " seconds");
                    return true;
                } catch (final Exception e) {
                    callback.error(e.getMessage());
                }
            } else if (action.equals("exitGTM")) {
                try {
                    inited = false;
                    callback.success("exitGTM");
                    return true;
                } catch (final Exception e) {
                    callback.error(e.getMessage());
                }
            } else if (action.equals("trackEvent")) {
                if(inited) {
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("event", args.getString(0), "eventCategory", args.getString(1), "eventAction", args.getString(2), "eventLabel", args.getString(3), "eventValue", args.getString(4), "eventNoninteraction", args.getString(5)));
                        callback.success("trackEvent("+args.getString(0)+") - eventCategory = " + args.getString(1) + "; eventAction = " + args.getString(2) + "; eventLabel = " + args.getString(3) + "; eventValue = " + args.getString(4) + "; eventNoninteraction = " + args.getString(5));
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("trackPage")) {
                if(inited) {
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("event", args.getString(0), "screenName", args.getString(1), "screenTitle", args.getString(2), "loggedIn", args.getString(3), "userId", args.getString(4)));
                        callback.success("trackEvent("+args.getString(0)+") - screenName = " + args.getString(1) + "; screenTitle = " + args.getString(2) + "; loggedIn = " + args.getString(3) + "; userId = " + args.getString(4));
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("trackSimpleEvent")) {
                if(inited) {
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("eventCategory", args.getString(0), "screenAction", args.getString(1), "eventLabel", args.getString(2)));
                        callback.success("eventCategory("+args.getString(0)+") - screenAction = " + args.getString(1) + "; eventLabel = " + args.getString(2));
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("trackDevice")) {
                if(inited) {
                    // PackageManager manager = context.getPackageManager();
                    // PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
                    // String version = info.versionName;
                    String version = "2.0.2";

                    // Info adInfo = null;
                    String aaid = "test";

                    // try {
                    //     Context ctx = this.cordova.getActivity().getApplicationContext();// this.cordova.getActivity().getApplicationContext();
                    //     adInfo = AdvertisingIdClient.getAdvertisingIdInfo(ctx);
                    //     aaid = adInfo.getId();
                    // } catch (IOException e) {
                    //     e.printStackTrace();
                    // } catch (GooglePlayServicesRepairableException e) {
                    //     e.printStackTrace();
                    // } catch (GooglePlayServicesNotAvailableException e) {
                    //     e.printStackTrace();
                    // }

                    // AlertDialog alertDialog = new AlertDialog.Builder(this.cordova.getActivity().getApplicationContext()).create();
                    // alertDialog.setTitle("My test");
                    // alertDialog.setMessage(aaid);
                    // alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    //     new DialogInterface.OnClickListener() {
                    //         public void onClick(DialogInterface dialog, int which) {
                    //             dialog.dismiss();
                    //         }
                    //     });
                    // alertDialog.show();



                    //String aaid = adInfo.getId(); //AdvertisingIdClient.getAdvertisingIdInfo(context).getId();//AdvertisingInterface.getId(); //adInfo.getId();
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("device_advertiser_id", aaid, "device_app_version", version));
                        callback.success("device_advertiser_id = "+"aaid"+", device_app_version = " + "version");
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("trackUser")) {
                if(inited) {
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("user_params", DataLayer.mapOf("user_id", args.getString(0),"user_username", args.getString(1), "user_email", args.getString(2), "user_gender", args.getString(3), "user_postcode", args.getString(4), "user_birthday", args.getString(5), "user_permission_to_send_emails", args.getString(6), "user_created", args.getString(7), "user_marketo_id", args.getString(8), "user_firstname", args.getString(9), "user_lastname", args.getString(10), "user_email_confirmed", args.getString(11))));
                        callback.success("user_params(user_id = " + args.getString(0) + "; user_username = " + args.getString(1) + "; user_email = " + args.getString(2) + "; user_gender = " + args.getString(3) + "; user_postcode = " + args.getString(4) + "; user_birthday = " + args.getString(5) + "; user_permission_to_send_emails = " + args.getString(6) + "; user_created = " + args.getString(7) + "; user_marketo_id = " + args.getString(8) + "; user_firstname = " + args.getString(9) + "; user_lastname = " + args.getString(10) + "; user_email_confirmed = " + args.getString(11) + ")");
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("trackList")) {
                if(inited) {
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("list_params", DataLayer.mapOf("list_id", args.getString(0),"list_title", args.getString(1), "list_description_title", args.getString(2), "list_description_body", args.getString(3), "list_user_id", args.getString(4), "list_status", args.getString(5), "list_created", args.getString(6), "list_cover", args.getString(7))));
                        callback.success("list_params(list_id = " + args.getString(0) + "; list_title = " + args.getString(1) + "; list_description_title = " + args.getString(2) + "; list_description_body = " + args.getString(3) + "; list_user_id = " + args.getString(4) + "; list_status = " + args.getString(5) + "; list_created = " + args.getString(6) + "; list_cover = " + args.getString(7) + ")");
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("trackWish")) {
                if(inited) {
                    try {
                        DataLayer dataLayer = TagManager.getInstance(this.cordova.getActivity().getApplicationContext()).getDataLayer();
                        dataLayer.push(DataLayer.mapOf("wish_params", DataLayer.mapOf("wish_id", args.getString(0),"wish_url", args.getString(1), "wish_name", args.getString(2), "wish_description", args.getString(3), "wish_price", args.getString(4), "wish_image_url", args.getString(5), "wish_wish_list_id", args.getString(6), "wish_created", args.getString(7), "wish_status", args.getString(8))));
                        callback.success("wish_params(wish_id = " + args.getString(0) + "; wish_url = " + args.getString(1) + "; wish_name = " + args.getString(2) + "; wish_description = " + args.getString(3) + "; wish_price = " + args.getString(4) + "; wish_image_url = " + args.getString(5) + "; wish_wish_list_id = " + args.getString(6) + "; wish_created = " + args.getString(7) + "; wish_status = " + args.getString(8) + ")");
                        return true;
                    } catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("trackEvent failed - not initialized");
                }
            } else if (action.equals("dispatch")) {
                if (inited) {
                    try {
                        GAServiceManager.getInstance().dispatchLocalHits();
                    }
                    catch (final Exception e) {
                        callback.error(e.getMessage());
                    }
                }
                else {
                    callback.error("dispatch failed - not initialized");
                }
            }
            return false;
        }
}
