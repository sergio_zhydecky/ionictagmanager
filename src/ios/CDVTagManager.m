/**
 * Copyright (c) 2014 Jared Dickson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#import "CDVTagManager.h"

@implementation CDVTagManager
- (void) initGTM:(CDVInvokedUrlCommand*)command
{
    NSString    *callbackId = command.callbackId;
    NSString    *accountID = [command.arguments objectAtIndex:0];
    NSInteger   dispatchPeriod = [[command.arguments objectAtIndex:1] intValue];

    inited = FALSE;
    self.tagManager = [TAGManager instance];

    // Modify the log level of the logger to print out not only
    // warning and error messages, but also verbose, debug, info messages.
    [self.tagManager.logger setLogLevel:kTAGLoggerLogLevelVerbose];

    // Set the dispatch interval
    self.tagManager.dispatchInterval = dispatchPeriod;

    // Open a container.
    [TAGContainerOpener openContainerWithId:accountID
                                 tagManager:self.tagManager
                                   openType:kTAGOpenTypePreferNonDefault
                                    timeout:nil
                                   notifier:self];
    [self successWithMessage:[NSString stringWithFormat:@"initGTM: accountID = %@; Interval = %d seconds",accountID, dispatchPeriod] toID:callbackId];
}

- (void) containerAvailable:(TAGContainer *)container {
    // Important note: containerAvailable may be called from a different thread, marshall the
    // notification back to the main thread to avoid a race condition with viewDidAppear.
    inited = TRUE;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.container = container;
    });
}

-(void) exitGTM:(CDVInvokedUrlCommand*)command
{
    NSString *callbackId = command.callbackId;

    if (inited)
        [self.container close];

    [self successWithMessage:@"exitGTM" toID:callbackId];
}

- (void) trackEvent:(CDVInvokedUrlCommand*)command
    {
        NSString        *callbackId = command.callbackId;
        NSString        *eventName = [command.arguments objectAtIndex:0];
        NSString        *eventCategory = [command.arguments objectAtIndex:1];
        NSString        *eventAction = [command.arguments objectAtIndex:2];
        NSString        *eventLabel = [command.arguments objectAtIndex:3];
        NSString        *eventValue = [command.arguments objectAtIndex:4];
        NSString        *eventNoninteraction = [command.arguments objectAtIndex:5];


        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"event":eventName, @"eventCategory":eventCategory, @"eventAction":eventAction, @"eventLabel":eventLabel, @"eventValue":eventValue, @"eventNoninteraction": eventNoninteraction}];
        }
        else
        [self failWithMessage:@"trackEvent failed - not initialized" toID:callbackId withError:nil];
    }

- (void) trackPage:(CDVInvokedUrlCommand*)command
    {
        //    NSString            *callbackId = command.callbackId;
        //    NSString            *pageURL = [command.arguments objectAtIndex:0];
        //
        //    if (inited)
        //    {
        //        TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
        //        [dataLayer push:@{@"event": @"content-view", @"content-name":pageURL}];
        //    }
        //    else
        //        [self failWithMessage:@"trackPage failed - not initialized" toID:callbackId withError:nil];

        NSString        *callbackId = command.callbackId;
        NSString        *eventName = [command.arguments objectAtIndex:0];
        NSString        *screenName = [command.arguments objectAtIndex:1];
        NSString        *screenTitle = [command.arguments objectAtIndex:2];
        NSString        *loggedIn = [command.arguments objectAtIndex:3];
        NSString        *userId = [command.arguments objectAtIndex:4];

        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"event":eventName, @"screenName":screenName, @"screenTitle":screenTitle, @"loggedIn":loggedIn, @"userId":userId}];
            //[dataLayer push:@{@"event": @"openScreen", @"screenName": @"Home Screen"}];
        }
        else
        [self failWithMessage:@"trackPage failed - not initialized" toID:callbackId withError:nil];
    }

- (void) trackSimpleEvent:(CDVInvokedUrlCommand*)command
    {

        NSString        *callbackId = command.callbackId;
        NSString        *eventCategory = [command.arguments objectAtIndex:0];
        NSString        *eventAction = [command.arguments objectAtIndex:1];
        NSString        *eventLabel = [command.arguments objectAtIndex:2];

        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"eventCategory":eventCategory, @"screenAction":eventAction, @"eventLabel":eventLabel}];
        }
        else
        [self failWithMessage:@"trackSimpleEvent failed - not initialized" toID:callbackId withError:nil];
    }

- (void) trackDevice:(CDVInvokedUrlCommand*)command
    {

        NSString        *callbackId = command.callbackId;
        NSString        *deviceAdvertiseerId = [self getIDFA];//[command.arguments objectAtIndex:0];tcyc
        NSString        *deviceAppVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];//[command.arguments objectAtIndex:1];

        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"device_params":@{@"device_advertiser_id":deviceAdvertiseerId, @"device_app_version":deviceAppVersion}}];
        }
        else
        [self failWithMessage:@"trackDevice failed - not initialized" toID:callbackId withError:nil];
    }
- (NSString*)getIDFA
    {


        // throw error if on iOS < 6.0
        if (NSClassFromString(@"ASIdentifierManager")) {
            NSString *advertiserID = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
            NSLog(@"output is : %@", advertiserID);
//            NSString *advertiserID = [[(id)[NSClassFromString(@"ASIdentifierManager") sharedManager] advertisingIdentifier] UUIDString];
            //NSString *advertiserID = (id)[NSClassFromString(@"ASIdentifierManager") UUIDString];
            // have to handle iOS bug where 00000000-0000-0000-0000-000000000000 may be returned on iOS 6.0
            if (advertiserID != nil && [advertiserID length] > 0 && ![advertiserID isEqualToString:@"00000000-0000-0000-0000-000000000000"]) {
                return advertiserID;
            }
        }


        return nil;
    }

- (void) trackUser:(CDVInvokedUrlCommand*)command
    {

        NSString        *callbackId = command.callbackId;
        NSString        *userId = [command.arguments objectAtIndex:0];
        NSString        *userUsername = [command.arguments objectAtIndex:1];
        NSString        *userEmail = [command.arguments objectAtIndex:2];
        NSString        *userGender = [command.arguments objectAtIndex:3];
        NSString        *userPostcode = [command.arguments objectAtIndex:4];
        NSString        *userBirthday = [command.arguments objectAtIndex:5];
        NSString        *userPermisionToSendEmail = [command.arguments objectAtIndex:6];
        NSString        *userCreated = [command.arguments objectAtIndex:7];
        NSString        *userMarketoId = [command.arguments objectAtIndex:8];
        NSString        *userFirstname = [command.arguments objectAtIndex:9];
        NSString        *userLastname = [command.arguments objectAtIndex:10];
        NSString        *userEmailConfirmed = [command.arguments objectAtIndex:11];

        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"user_params":@{@"user_id":userId, @"user_username":userUsername, @"user_email":userEmail, @"user_gender":userGender, @"user_postcode":userPostcode, @"user_birthday":userBirthday, @"user_permission_to_send_emails":userPermisionToSendEmail, @"user_created":userCreated, @"user_marketo_id":userMarketoId, @"user_firstname":userFirstname, @"user_lastname":userLastname, @"user_email_confirmed":userEmailConfirmed}}];
        }
        else
        [self failWithMessage:@"trackUser failed - not initialized" toID:callbackId withError:nil];
    }

- (void) trackList:(CDVInvokedUrlCommand*)command
    {

        NSString        *callbackId = command.callbackId;
        NSString        *listId = [command.arguments objectAtIndex:0];
        NSString        *listTitle = [command.arguments objectAtIndex:1];
        NSString        *listDescriptionTitle = [command.arguments objectAtIndex:2];
        NSString        *listDescriptionBody = [command.arguments objectAtIndex:3];
        NSString        *listUserId = [command.arguments objectAtIndex:4];
        NSString        *listStatus = [command.arguments objectAtIndex:5];
        NSString        *listCreated = [command.arguments objectAtIndex:6];
        NSString        *listCover = [command.arguments objectAtIndex:7];

        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"list_params":@{@"list_id":listId, @"list_title":listTitle, @"list_description_title":listDescriptionTitle, @"list_description_body":listDescriptionBody, @"list_user_id":listUserId, @"list_status":listStatus, @"list_created":listCreated, @"list_cover":listCover}}];
        }
        else
        [self failWithMessage:@"trackUser failed - not initialized" toID:callbackId withError:nil];
    }

- (void) trackWish:(CDVInvokedUrlCommand*)command
    {

        NSString        *callbackId = command.callbackId;
        NSString        *wishId = [command.arguments objectAtIndex:0];
        NSString        *wishUrl = [command.arguments objectAtIndex:1];
        NSString        *wishName = [command.arguments objectAtIndex:2];
        NSString        *wishDescription = [command.arguments objectAtIndex:3];
        NSString        *wishPrice = [command.arguments objectAtIndex:4];
        NSString        *wishImageUrl = [command.arguments objectAtIndex:5];
        NSString        *wishWishListId = [command.arguments objectAtIndex:6];
        NSString        *wishCreated = [command.arguments objectAtIndex:7];
        NSString        *wishStatus = [command.arguments objectAtIndex:8];

        if (inited)
        {
            TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
            [dataLayer push:@{@"wish_params":@{@"wish_id":wishId, @"wish_url":wishUrl, @"wish_name":wishName, @"wish_description":wishDescription, @"wish_price":wishPrice, @"wish_image_url":wishImageUrl, @"wish_wish_list_id":wishWishListId, @"wish_created":wishCreated, @"wish_status":wishStatus}}];
        }
        else
        [self failWithMessage:@"trackUser failed - not initialized" toID:callbackId withError:nil];
    }

- (void) dispatch:(CDVInvokedUrlCommand*)command
{
    NSString            *callbackId = command.callbackId;

    if (inited)
    {
        [self.tagManager dispatch];
    }
    else
        [self failWithMessage:@"dispatch failed - not initialized" toID:callbackId withError:nil];
}

-(void) successWithMessage:(NSString *)message toID:(NSString *)callbackID
{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];

    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackID];
}

-(void) failWithMessage:(NSString *)message toID:(NSString *)callbackID withError:(NSError *)error
{
    NSString        *errorMessage = (error) ? [NSString stringWithFormat:@"%@ - %@", message, [error localizedDescription]] : message;
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];

    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackID];
}

-(void)dealloc
{
    [self.container close];
    // [super dealloc];
}

@end
