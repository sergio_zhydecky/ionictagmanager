cordova.define("dk.ovdal.tag-manager.TagManager", function(require, exports, module) {
    (function () {
        var cordovaRef = window.PhoneGap || window.cordova || window.Cordova;
        var queue = [];
        var runInterval = 1000;
        var running = false;
        var runner;

        function TagManager() {}

        // initialize google analytics with an account ID and the min number of seconds between posting
        //
        // id = the GTM account ID of the form 'GTM-000000'
        // period = the minimum interval for transmitting tracking events if any exist in the queue
        TagManager.prototype.init = function (success, fail, id, period) {
            runner = setInterval(run, runInterval);
            running = true;
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'initGTM',
                success: success,
                fail: fail,
                id: id,
                period: period
            });
        };

        TagManager.prototype.trackDevice = function (success, fail){
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackDevice',
                success: success,
                fail: fail
            });
        };

        TagManager.prototype.trackUser = function (success, fail, userId, userUsername, userEmail, userGender, userPostcode, userBirthday, userPermisionToSendEmail, userCreated, userMarketoId, userFirstname, userLastname, userEmailConfirmed){
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackUser',
                success: success,
                fail: fail,
                userId: userId,
                userUsername: userUsername,
                userEmail: userEmail,
                userGender: userGender,
                userPostcode: userPostcode,
                userBirthday: userBirthday,
                userPermisionToSendEmail: userPermisionToSendEmail,
                userCreated: userCreated,
                userMarketoId: userMarketoId,
                userFirstname: userFirstname,
                userLastname: userLastname,
                userEmailConfirmed: userEmailConfirmed
            });
        };
        TagManager.prototype.trackList = function (success, fail, listId, listTitle, listDescriptionTitle, listDescriptionBody, listUserId, listStatus, listCreated, listCover){
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackList',
                success: success,
                fail: fail,
                listId: listId,
                listTitle: listTitle,
                listDescriptionTitle: listDescriptionTitle,
                listDescriptionBody: listDescriptionBody,
                listUserId: listUserId,
                listStatus: listStatus,
                listCreated: listCreated,
                listCover: listCover
            });
        }

        TagManager.prototype.trackWish = function(success, fail, wishId, wishUrl, wishName, wishDescription, wishPrice, wishImageUrl, wishWishListId, wishCreated, wishStatus){
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackWish',
                success: success,
                fail: fail,
                wishId: wishId,
                wishName: wishName,
                wishDescription: wishDescription,
                wishPrice: wishPrice,
                wishImageUrl: wishImageUrl,
                wishWishListId: wishWishListId,
                wishCreated: wishCreated,
                wishStatus: wishStatus
            });
        }

        TagManager.prototype.trackEvent = function (success, fail, eventName, eventCategory, eventAction, eventLabel, eventValue, eventNoninteraction) {
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackEvent',
                success: success,
                fail: fail,
                eventName: eventName,
                eventCategory: eventCategory,
                eventAction: eventAction,
                eventLabel: eventLabel,
                eventValue: eventValue,
                eventNoninteraction: eventNoninteraction
            });
        };

        // log a page view
        //
        // pageURL = the URL of the page view
        TagManager.prototype.trackPage = function (success, fail, eventName, screenName, screenTitle, loggedIn, userId) {
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackPage',
                success: success,
                fail: fail,
                eventName: eventName,
                screenName: screenName,
                screenTitle: screenTitle,
                loggedIn: loggedIn,
                userId: userId

            });
        };

        TagManager.prototype.trackSimpleEvent = function (success, fail, eventCategory, eventAction, eventLabel){
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'trackSimpleEvent',
                success: success,
                fail: fail,
                eventCategory: eventCategory,
                eventAction: eventAction,
                eventLabel: eventLabel
            });

        };







        // log an event
        //
        // category = The event category. This parameter is required to be non-empty.
        // eventAction = The event action. This parameter is required to be non-empty.
        // eventLabel = The event label. This parameter may be a blank string to indicate no label.
        // eventValue = The event value. This parameter may be -1 to indicate no value.
        // TagManager.prototype.trackEvent = function (success, fail, category, eventAction, eventLabel, eventValue) {
        // 	var timestamp = new Date().getTime();
        // 	queue.push({
        // 		timestamp: timestamp,
        // 		method: 'trackEvent',
        // 		success: success,
        // 		fail: fail,
        // 		category: category,
        // 		eventAction: eventAction,
        // 		eventLabel: eventLabel,
        // 		eventValue: eventValue
        // 	});
        // };

        TagManager.prototype.pushEvent = function (success, fail, eventData) {
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'pushEvent',
                success: success,
                fail: fail,
                eventData: eventData
            });
        };

        // log a page view
        //
        // pageURL = the URL of the page view
        // TagManager.prototype.trackPage = function (success, fail, pageURL) {
        // 	var timestamp = new Date().getTime();
        // 	queue.push({
        // 		timestamp: timestamp,
        // 		method: 'trackPage',
        // 		success: success,
        // 		fail: fail,
        // 		pageURL: pageURL
        // 	});
        // };

        // force a dispatch to Tag Manager
        TagManager.prototype.dispatch = function (success, fail) {
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'dispatch',
                success: success,
                fail: fail
            });
        };

        // exit the TagManager instance and stop setInterval
        TagManager.prototype.exit = function (success, fail) {
            var timestamp = new Date().getTime();
            queue.push({
                timestamp: timestamp,
                method: 'exitGTM',
                success: success,
                fail: fail
            });
        };

        if (cordovaRef && cordovaRef.addConstructor) {
            cordovaRef.addConstructor(init);
        } else {
            init();
        }

        function init() {
            if (!window.plugins) {
                window.plugins = {};
            }
            if (!window.plugins.TagManager) {
                window.plugins.TagManager = new TagManager();
            }
        }

        function run() {
            if (queue.length > 0) {
                var item = queue.shift();
                if (item.method === 'initGTM') {
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.id, item.period]);
                }
                else if (item.method === 'trackEvent') {
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.eventName, item.eventCategory, item.eventAction, item.eventLabel, item.eventValue, item.eventNoninteraction]);
                }
                else if (item.method === 'trackPage') {
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.eventName, item.screenName, item.screenTitle, item.loggedIn, item.userId]);
                }
                else if (item.method === 'dispatch') {
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, []);
                }
                else if (item.method === 'exitGTM') {
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, []);
                    clearInterval(runner);
                    running = false;
                }
                else if (item.method === 'trackSimpleEvent'){
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.eventCategory, item.eventAction, item.eventLabel]);
                }
                else if (item.method === 'trackDevice'){
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, []);
                }
                else if (item.method === 'trackUser'){
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.userId, item.userUsername, item.userEmail, item.userGender, item.userPostcode, item.userBirthday, item.userPermisionToSendEmail, item.userCreated, item.userMarketoId, item.userFirstname, item.userLastname, item.userEmailConfirmed]);
                }
                else if (item.method === 'trackList'){
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.listId, item.listTitle, item.listDescriptionTitle, item.listDescriptionBody, item.listUserId, item.listStatus, item.listCreated, item.listCover]);
                }
                else if (item.method === 'trackWish'){
                    cordovaRef.exec(item.success, item.fail, 'TagManager', item.method, [item.wishId, item.wishUrl, item.wishName, item.wishDescription, item.wishPrice, item.wishImageUrl, item.wishWishListId, item.wishCreated, item.wishStatus]);
                }
            }
        }

        if (typeof module != 'undefined' && module.exports) {
            module.exports = new TagManager();
        }
    })();
	/* End of Temporary Scope. */
});
